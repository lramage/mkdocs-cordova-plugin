FROM runmymind/docker-android-sdk:alpine-standalone

WORKDIR /usr/src/mkdocs-cordova-plugin
COPY . /usr/src/mkdocs-cordova-plugin

RUN apk update && apk upgrade && \
    apk add gcc \
            gradle \
            libffi-dev \
            make \
            musl-dev \
            npm \
            openssl-dev \
            py3-pip \
            py3-setuptools \
            python \
            python3-dev && \
    python3 -m pip install --upgrade pip && \
    pip install mkdocs setuptools && \
    npm install -g cordova && \
    python3 setup.py install

CMD ["mkdocs", "build"]
