# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://mkdocs.org).

<a href="https://gitlab.com/lramage94/mkdocs-cordova-plugin/-/jobs/artifacts/master/raw/public/org.mkdocs.cordova.apk?job=pages">
<img alt="Get it on gitlab-ci" style="width: 200px;" src="img/get-it-on.svg"></a>

## Installation

    pip install mkdocs-cordova-plugin

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml                      # The configuration file.
    docs/
        index.md                    # The documentation homepage.
        ...                         # Other markdown pages, images and other files.
    cordova/index.js                # Apache Cordova Application logic (required)
    cordova/templates/config.xml    # Apache Cordova Application global configuration file
    cordova/templates/main.html     # Jinja2 Template for Apache Cordova
    cordova/templates/package.json  # (optional; needed to publish template on npm)
