# Overriding the Favicon

## Configuration

Add the following to the `mkdocs.yml` file:

```yaml
theme:
  custom_dir: docs/theme
```

## Template

Create a `docs/theme/main.html` file with the following contents:

```html
{% extends "base.html" %}

{%- block site_meta %}
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
{% if page and page.is_homepage %}<meta name="description" content="{{ config.site_description }}">{% endif %}
{% if config.site_author %}<meta name="author" content="{{ config.site_author }}">{% endif %}
{% if page and page.canonical_url %}<link rel="canonical" href="{{ page.canonical_url }}">{% endif %}
<link rel="shortcut icon" href="{{ 'res/icon.png' | url }}">
{%- endblock %}
```

### Reference

- [Customize Icons](https://cordova.apache.org/docs/en/latest/config_ref/images.html)

