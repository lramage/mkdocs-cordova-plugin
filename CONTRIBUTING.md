# Contributing to MkDocs

See the contributing guide in the documentation for an
introduction to contributing to MkDocs.

<https://www.mkdocs.org/about/contributing/>

## Releasing on PyPI

First update the package version in both the [`setup.py`](setup.py), and [`mkdocs.yml`](mkdocs.yml) files,
then run the following commands to generate the release and upload:

```sh
python3 setup.py sdist --formats gztar

twine upload dist/*
```
